<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>logs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Bootstrap 3 template for corporate business" />
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
</head>

<body>
<?php include ('DataService.php')?>
<div id="wrapper">
    <!-- start header -->
    <header>
        <div class="navbar navbar-default navbar-static-top">
            <center><h1>EXAMINATION VERIFICATION SYSTEM</h1></center>
        </div>
    </header>
    <!-- end header -->

    <section id="content">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="widget">
                        <form role="form">
                            <div class="form-group pull-left">
                                <label>Course Code:  </label>
                                <input type="text" id="s" placeholder="Search course code...">
                            </div>
                            <div class="form-group pull-right">
                                <label>Academic Year:   </label>
                                <input type="text" id="" placeholder="Select Year...">
                                <button>View Logs</button>
                            </div>
                        </form>

                    </div>
                    <article>
                        <div >

                            <table class="table table-bordered" style="margin-top: 60px">
                                <thead>
                                <tr style="background:#c1e2b3">
                                    <th>Course Code</th>
                                    <th>Exam Date</th>
                                    <th>Venue</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                while ($row = mysqli_fetch_assoc($exams)){
                                    $code = $row['course_code'];
                                    $date = $row['exam_date'];
                                    $venue = $row['venue'];
                                    ?>
                                <tr>
                                    <td><?php echo $code ?></td>
                                    <td><?php echo $date ?></td>
                                    <td><?php echo $venue ?></td>


                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>

                        </div>
                    </article>

                </div>
            </div>
        </div>
    </section>

</div>


</body>

</html>
