<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>student</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="css/bootstrap.min.css" rel="stylesheet" />
	<link href="css/style.css" rel="stylesheet" />
</head>

<body>

<?php
include('DataService.php')
?>

	<div id="wrapper">
		<!-- start header -->
		<header>
			<div class="navbar navbar-default navbar-static-top">
						<center><h1>EXAMINATION VERIFICATION SYSTEM</h1></center>
			</div>
		</header>
		<!-- end header -->

		<section id="content">
			<div class="container">
				<div class="row">

					<div class="col-lg-4">
						<aside class="left-sidebar">
							<div class="widget">
                                <img src="img/img1.jpg" alt="" class="img-responsive" width="200" height="400"/>
								<p>Name: NAGGINDA MARTHA</p>
								<p>Student Number: 215013711</p>
								<p>Reg No: 15/U/9170/PS</p>
								<p>Program: DAY</p>
								<p>SChool: College of computing and information technology</p>
							</div>

						</aside>
					</div>

					<div class="col-lg-8">
						<div class="widget">
							<form role="form">
								<div class="form-group">
									<input type="text" class="form-control" id="s" placeholder="Search..">
								</div>
							</form>
						</div>
						<article>
							<div class="post-image">
								<div class="post-heading">
									<p>Subjects Registered</p>

								</div>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Course Code</th>
                                        <th>Course Name</th>
                                        <th>DAY</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    <?php
                                    $i=0;
                                    while($row = mysqli_fetch_assoc($units)) {
                                        $code = $row['course_code'];
                                        $name = $row['course_name'];
                                    ?>
                                    <tr>
                                        <td><?php echo $code; ?></td>
                                        <td><?php echo $name; ?></td>
                                        <td>DAY</td>
                                    </tr>
                                        <?php
                                        $i++;
                                    }?>

                                    </tbody>
                                </table>
                              <strong> <center> <P>ACCOUNT BALANCE</P>
								  <p>ugx: 5000</p></center></strong>
							</div>
						</article>

					</div>
				</div>

			</div>
		</section>

	</div>


</body>

</html>

